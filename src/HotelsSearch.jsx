import React from "react";
import axios from "axios";
import { useState, useEffect } from "react";

const HotelSearch = () => {
    const [searchTerm, setSearchTerm] = useState([]);
    const [searchLon, setSearchLon] = useState([]);
    const [searchLat, setSearchLat] = useState([]);
    const [hotels, setHotels] = useState([]);
    const token = "a6efa997ef26c712aa8ecfa46a9f6cf9";
    useEffect(() => {
	setSearchTerm("montreal");
	axios
	    .get(
		`http://localhost:5173/api?query=montreal&lang=fr&lookFor=both&limit=25&token=${token}`
	    )
	    .then(
		(res) => {
		setHotels(res.data.results.hotels);
		}
	    )
	    .catch((err) => {
		console.log(err);
	    }
		  );
    }, []);
    
function searchHotel(){
    axios
	.get(
	    `http://localhost:5173/api?query=${searchTerm}&lang=fr&lookFor=both&limit=25&token=${token}`
	)
	.then(
	    (res) => {
		setHotels(res.data.results.hotels);
	    }
	)
	.catch((err) => {
	    console.log(err);
	}
	      );
}


function searchHotelByCoordinates(){
    axios
	.get(
	    `http://localhost:5173/api?query=${searchLat},${searchLon}&lang=fr&lookFor=both&limit=25&token=${token}`
	)
	.then(
	    (res) => {
		var hotels = res.data.results.hotels.map(
		    hotel => ({
			"fullName": hotel.name,
			"location": hotel.location,
			"locationName": null
		    })
		);
		setHotels(hotels);
	    }
	)
	.catch((err) => {
	    console.log(err);
	}
	      );
}
  var hotelList = hotels.map((hotel, key) => 
  <div key = {key} >
    <p><b>{hotel.fullName}</b></p>
      <p>Ville: <span>{hotel.locationName}</span></p>
      <p>lat: {hotel.location.lat} lon: {hotel.location.lon}</p>
    <hr />
  </div>);

  return (
        <div>
          <div><label>Entrez un mot clé:</label></div>
            <div><input value={searchTerm} onChange={(e) => {setSearchTerm(e.target.value);}} /></div>
	    <div><button onClick={searchHotel}>Rechercher</button></div>
	    <div>Recherche par coordonnées</div>
	    <div><label>Latitude:</label></div>
	    <div><input value={searchLat} onChange={(e) => {setSearchLat(e.target.value)}}/></div>
	    <div><label>Longitude:</label></div>
	    <div><input value={searchLon} onChange={(e) => {setSearchLon(e.target.value)}}/></div>
	    <div><button onClick={searchHotelByCoordinates}>Recherche par coordonnées</button></div>
		
	    
          {hotelList}
        </div>);
};

export default HotelSearch;
